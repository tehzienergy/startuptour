jQuery(document).ready(function($) {
  
  var slider = $('.live-sessions')
  
  if (slider.length) {
    slider.each(function (e) {
      $(this).slick({
        infinite: true,
        autoplay: false,
        dots: false,
        arrows: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              dots: true
            }
          },
          {
            breakpoint: 576,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          },
        ]
      })
    })
  }

})
