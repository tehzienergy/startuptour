$('.js-textarea-limited').on("input", function () {
  var maxlength = $(this).attr("maxlength");
  var currentLength = $(this).val().length;

  if (currentLength >= maxlength) {
    $(this).closest('.form__textarea-wrapper').addClass('limit');
  } else {
    $(this).closest('.form__textarea-wrapper').removeClass('limit');
  }
});

$('.form__trigger').click(function(e) {
  e.preventDefault();
  $(this).closest('.form__trigger-wrapper').find('.form__trigger-content').fadeToggle();
  $(this).closest('.form__trigger-wrapper').find('.form__trigger-top').toggle();
})
