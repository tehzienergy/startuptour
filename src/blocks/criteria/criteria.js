jQuery(document).ready(function($) {
  
  $('.criteria__header').on('click', function (e) {
    e.preventDefault()
    $(this).toggleClass('is-active')
    $(this).siblings('.criteria__content').slideToggle(250)
  })
  
})
