jQuery(document).ready(function($) {

  var popupClose = function () {
    $('.popup').addClass('is-hidden').delay(500).queue(function(next) {
      $(this).removeClass('is-active')
      $(this).removeClass('is-hidden')
      next()
    })
    $('.filter').removeAttr('style')
  }

  $('.filter__btn').on('click', function (e) {
    e.preventDefault()
    
    if ( $(this).siblings('.popup').hasClass('is-active') ) {
      popupClose()
    } else {
      $('.filter__btn').not(this).each(function() {
        $(this).siblings('.popup').addClass('is-hidden').delay(500).queue(function(next) {
          $(this).removeClass('is-active')
          $(this).removeClass('is-hidden')
          next()
        })
      })
      $(this).siblings('.popup').addClass('is-active')
      $('.filter').removeAttr('style')
      $(this).parents('.filter').css({
        'z-index': 2
      })
    }
  })

  $(document).on('click', function (e) {
    if (!$(e.target).hasClass('filter') && $(e.target).parents('.filter').length === 0) {
      popupClose()
    }
  })

})
