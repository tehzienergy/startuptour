jQuery(document).ready(function($) {
  
  var slider = $('.slider')
  
  if (slider.length) {
    slider.each(function (e) {
      $(this).slick({
        infinite: true,
        autoplay: false,
        dots: false,
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        variableWidth: true,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
            }
          },
          {
            breakpoint: 576,
            settings: {
              variableWidth: false,
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
            }
          },
        ]
      })
    })
  }

})
