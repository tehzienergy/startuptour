$('.accordion__header').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('accordion__header--active');
  $(this).closest('.accordion').find('.accordion__content').toggle();
})
