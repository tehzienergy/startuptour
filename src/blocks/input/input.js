jQuery(document).ready(function($) {

  if ( !($('.form--white').length) ) {
    $('.input').on('blur', function (e) {
      var val = $(this).val()
      if (val !== '') {
        $(this).addClass('is-active')
      } else {
        $(this).removeClass('is-active')
      }
    })
  }

})
