jQuery(document).ready(function($) {
  
  var drag = $('.js-drag')
  
  if (drag.length) {

    var $frame = $('.drag__content');

    // Call Sly on frame
    $frame.sly({
      horizontal: 1,
      itemNav: 'basic',
      smart: 1,
      activateOn: 'click',
      mouseDragging: 1,
      touchDragging: 1,
      releaseSwing: 1,
      startAt: 0,
      scrollBar: $('.drag__scrollbar'),
      scrollBy: 1,
      speed: 600,
      elasticBounds: 1,
      dragHandle: 1,
      dynamicHandle: 1,
      clickBar: 1,
      swingSpeed: 0.2,
      keyboardNavBy: 'items',
    })
    
  }

})
