jQuery(document).ready(function ($) {

  var selectWrapper = $('.select')
  var select = $('.select__input')

  var selectSettings = {
    minimumResultsForSearch: -1,
    width: '100%',
    maximumSelectionLength: 2,
    escapeMarkup: function (markup) {
      return markup
    }
  }

  if (selectWrapper.length) {
    if ($('.form--white').length) {
      var selectSettingsUpdated = $.extend(selectSettings, {
        dropdownCssClass: 'select2-dropdown--dark'
      })
      select.select2(selectSettingsUpdated)
    } else {
      select.select2(selectSettings)
    }
  }

})
