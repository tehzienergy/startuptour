jQuery(document).ready(function($) {

  if ($('.popup__content').length) {
    $('.popup__content').each(function() {
      var ps = new PerfectScrollbar($(this)[0], {
        wheelSpeed: 2,
        wheelPropagation: false,
      })
    })
  }

})
